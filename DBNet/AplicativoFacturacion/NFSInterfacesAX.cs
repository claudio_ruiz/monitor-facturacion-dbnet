﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Timers;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.Threading;
using System.Configuration;
using AplicativoFacturacion.Models;
using AplicativoFacturacion.NFSseInterfaceConfiguration;
using DataDAL;
using MonitorDTO;

namespace AplicativoFacturacion
{
    public partial class NFSInterfacesAX : Form
    {
        #region Hilos y Delegados
        LogFile lf = new LogFile();
        static System.Timers.Timer hilo1 = new System.Timers.Timer();
        static System.Timers.Timer hilo2 = new System.Timers.Timer();
        static System.Timers.Timer hilo3 = new System.Timers.Timer();
        delegate void DisplayEstado(string msg);
        delegate void CleanDisplay();
        delegate int GetIntervalTime();
        delegate void DisplayFtpDGV(List<FtpDTO> list);
        delegate string GetObjetValue(string type);
        ServicioConfiguracion wsService = new ServicioConfiguracion();
        List<NFSclParamConfigFacturacionDTO> envoiceList = new List<NFSclParamConfigFacturacionDTO>();
        int processMinutes = 1;
        #endregion
        public NFSInterfacesAX()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void btn_start_Click(object sender, System.EventArgs e)
        {         
            btn_start.Enabled = false;
            hilo1.Enabled = true;
            hilo1.Start();
            hilo1.Interval =  1000;
            hilo1.Elapsed += new ElapsedEventHandler(primer_proceso);
            hilo1.AutoReset = true;

        }

        private void primer_proceso(object myObject, EventArgs myEventArgs)
        {
            processMinutes = Convert.ToInt32(this.Invoke(new GetIntervalTime(GetTimeIteration)));
            hilo1.Stop();
            #region Este hilo va a cargar la lista de las facturas
            try
            {
                this.Invoke(new DisplayEstado(Progreso), "CARGANDO LISTA DE INTERFACES");
                LoadList();
                Thread.Sleep(1000 * (60 * processMinutes));
                hilo1.Enabled = true;
            }
            catch (Exception ex)
            {
            }
            #endregion
            hilo1.Enabled = true;
            hilo1.AutoReset = true;
        }      

        private void tercer_proceso(object myObject, EventArgs myEventArgs)
        {
            FTPDAL f = new FTPDAL();
            hilo3.Stop();
        }
        private void LoadList()
        {
            lock (hilo1)
            {
                envoiceList = wsService.GetInterfaceConfiguration(1);// si no se agrega valor al metodo retorna solo facturas

                if (envoiceList != null)
                {
                    this.Invoke(new DisplayEstado(Progreso), "PROCESO DE FACTURAS/GUIAS");
                    if (envoiceList.Count > 0)
                    {
                        foreach (var item in envoiceList.Where(x => x.idLaboratorio != "02" &&
                                                              (x.idInterfaz == "1" || x.idInterfaz == "6"
                                                              || x.idInterfaz == "7" || x.idInterfaz == "4"
                                                              || x.idInterfaz == "8")))
                        {
                            this.Invoke(new DisplayEstado(Progreso), "PREPARANDO " + item.tipoDoc + " TIPO REGISTRO " + item.tipoRegistro + " LABORATORIO " + item.idLaboratorio);
                            if (item.tipoRegistro == "0")
                            {
                                runZeroType(item);
                                this.Invoke(new DisplayEstado(Progreso), item.tipoDoc + " TIPO REGISTRO " + item.tipoRegistro + " TERMINADO");
                            }
                            else
                            {
                                runTypes(item);
                                this.Invoke(new DisplayEstado(Progreso), item.tipoDoc + " TIPO REGISTRO " + item.tipoRegistro + " TERMINADO");
                            }
                            this.Invoke(new DisplayEstado(Progreso), "DESCANSO PROGRAMADO ");
                        }
                    }
                    else
                    {
                        this.Invoke(new DisplayEstado(Progreso), "Sin conección a AX, Revisar AOS correspondiente");
                    }
                }
                else
                {
                    this.Invoke(new DisplayEstado(Progreso), "Lista de facturas vacia , datos borrados o error de conexión con el servidor");
                }
            }
           
        }

        private void btn_cancel_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("¿Desea cancelar el proceso de actualización de FTP?", "Cancelación", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    #region
                    hilo1.Stop();
                    hilo1.Enabled = false;
                    hilo1.Dispose();
                    hilo1.Close();
                    #endregion
                }
                catch (Exception) { }
                btn_start.Enabled = true;
                this.Close();
            }
        }

        private void ValidateDirectory(string root)
        {
            if (!Directory.Exists(root))
            {
                this.Invoke(new DisplayEstado(Progreso), "DIRECTORIO " + root + " NO EXISTE");
                Directory.CreateDirectory(root);
                this.Invoke(new DisplayEstado(Progreso), "DIRECTORIO " + root + " FUE CREADO CORRECTAMENTE");
            }
        }

        private void runZeroType(NFSclParamConfigFacturacionDTO conf)
        {
            try
            {               
                XmlDocument xDoc = new XmlDocument();
                string nombreArchivoFinal;
                string archivoActual = "";
                int counter = 0;
                int total = 0;

                ValidateDirectory(conf.rutaInicialXML);
                ValidateDirectory(conf.rutaInicialXML + "\\Respaldo\\");
                ValidateDirectory(conf.rutaFinalXML);
                
                 string[] xmlFiles = GetFileNames(conf.rutaInicialXML, "*.xml");// RESCATA LOS ARCHIVOS XML      

                if (xmlFiles.Length > 0)
                {
                    total = xmlFiles.Length;
                    this.Invoke(new DisplayEstado(Progreso), "SE ENCONTRARON " + total + " ARCHIVOS NUEVOS  EN " + conf.rutaInicialXML);
                    for (int i = 0; i < total; i++)
                    {
                        archivoActual = xmlFiles[i];
                        this.Invoke(new DisplayEstado(Progreso), "ARCHIVO A PROCESAR" + conf.rutaInicialXML + "\\" + archivoActual);
                        XslTransform xsl = new XslTransform();

                        if (!String.IsNullOrEmpty(conf.rutaXslt) && conf.rutaXslt.ToUpper().EndsWith(".XSLT"))
                        {
                            try
                            {
                                xsl.Load(conf.rutaXslt);
                            }
                            catch (Exception ex)
                            {
                               this.Invoke(new DisplayEstado(Progreso), "ERROR RUTA XSLT " + ex.Message);
                            }
                        }

                        if (conf.idLaboratorio == "49" && conf.idInterfaz != "5")
                        {
                            xDoc.Load(conf.rutaInicialXML + "\\" + archivoActual);
                            XmlNodeList records = xDoc.GetElementsByTagName("Records");
                            XmlNodeList record = ((XmlElement)records[0]).GetElementsByTagName("Record");
                            XmlNodeList custInvoiceJour = ((XmlElement)record[0]).GetElementsByTagName("CustInvoiceJour");
                            XmlNodeList salesTable = ((XmlElement)record[0]).GetElementsByTagName("SalesTable");
                            XmlNodeList accSalesOrderType = ((XmlElement)salesTable[0]).GetElementsByTagName("ACCsalesOrderType");
                            XmlNodeList invoiceId = ((XmlElement)custInvoiceJour[0]).GetElementsByTagName("InvoiceId");
                            XmlNodeList invoiceAccount = ((XmlElement)custInvoiceJour[0]).GetElementsByTagName("InvoiceAccount");

                            if (accSalesOrderType[0].InnerText.Substring(2, 1) == "F" && invoiceAccount[0].InnerText != "49-081201000K")
                            {
                                nombreArchivoFinal = "Factura" + invoiceId[0].InnerText.Substring(4, invoiceId[0].InnerText.Length - 4);
                            }
                            else if (accSalesOrderType[0].InnerText.Substring(2, 1) == "G" && invoiceAccount[0].InnerText != "49-081201000K")
                            {
                                nombreArchivoFinal = "Guia" + invoiceId[0].InnerText.Substring(4, invoiceId[0].InnerText.Length - 4);
                            }
                            else if (invoiceAccount[0].InnerText == "49-081201000K")
                            {
                                nombreArchivoFinal = "Jumbo" + invoiceId[0].InnerText.Substring(4, invoiceId[0].InnerText.Length - 4);
                            }
                            else
                            {
                                nombreArchivoFinal = accSalesOrderType[0].InnerText;
                            }
                        }
                        else
                        {
                            nombreArchivoFinal = DateTime.Now.ToString(conf.nombreArchivoXML);
                        }

                        String xmlPath = conf.rutaInicialXML + "\\" + archivoActual;
                        String[] xmlPathData = xmlPath.Split('_');
                        String folio = String.Empty;
                        if (xmlPathData.Count(s => s.Contains("folio$")) > 0)
                        {
                            folio = xmlPathData.Single(s => s.Contains("folio$"));
                            folio = folio.Replace("folio$", "");
                        }
                        nombreArchivoFinal = nombreArchivoFinal.Replace("$folio", folio);
                        //Si tiene ruta Xslt
                        if (!String.IsNullOrEmpty(conf.rutaXslt) && conf.rutaXslt.ToUpper().EndsWith(".XSLT") && xmlPath.ToUpper().EndsWith("XML"))
                        {
                            xsl.Transform(xmlPath, conf.rutaFinalXML + "\\" + nombreArchivoFinal + conf.formatoDoc);//fechaActualDia + fechaActualMes + fechaActualAño + "_" + fechaActualHora + fechaActualMinuto + fechaActualSegundo + conf.formato);//+ ((FileInfo)files[i]).Name.Replace(".xml", conf.formato));
                            Thread.Sleep(1000); // descanso entre cada documento generado para que el servidor lo procese
                        }
                        else if (!String.IsNullOrEmpty(conf.rutaXslt) && conf.rutaXslt.ToUpper().EndsWith(".XLS") && xmlPath.ToUpper().EndsWith("XML"))
                        {
                            InterfacesMerck.CreaExcelStock(xmlPath, conf.rutaXslt, conf.rutaFinalXML + "\\" + nombreArchivoFinal + conf.formatoDoc);
                        }
                        else
                        {
                            File.Copy(xmlPath, conf.rutaFinalXML + "\\" + nombreArchivoFinal + conf.formatoDoc);
                        }

                        this.Invoke(new DisplayEstado(Progreso), "ARCHIVO GENERADO CORRECTAMENTE");

                        if (File.Exists(conf.rutaInicialXML + "\\Respaldo\\" + archivoActual))
                        {
                            File.Delete(conf.rutaInicialXML + "\\Respaldo\\" + archivoActual);
                        }
                        else
                        {
                            File.Move(conf.rutaInicialXML + "\\" + archivoActual, conf.rutaInicialXML + "\\Respaldo\\" + archivoActual);
                        }
                        try
                        {
                            if (string.IsNullOrEmpty(conf.nomInterfaz))
                                throw new Exception("La interface para el laboratorio: " + conf.idLaboratorio.ToString() + " no contiene un nombre de interfaz definido en la tabla, debe definirse en Administracion de sistema -> Configuracion de interfaces ");
                            string nomarch;
                            if (!nombreArchivoFinal.Contains('.'))
                                nomarch = nombreArchivoFinal + conf.formatoDoc;
                            else nomarch = nombreArchivoFinal;

                            try
                            {
                                FtpDTO ftpItem = new FtpDTO();
                                FTPDAL ftpDal  = new FTPDAL();
                                ftpItem.Ftp = conf.nomInterfaz;
                                ftpItem.Path = conf.rutaFinalXML + @"\";
                                ftpItem.Name = nomarch;
                                ftpItem.Status = "CRE";
                                ftpItem.ProcessName = "Creacion archivos desde servicio NFSInterfaceService";
                                ftpItem.Obs = $"Laboratorio { conf.idLaboratorio.ToString()} --> {archivoActual}";
                               
                                ftpDal.SetFtpRecord(ftpItem);
                            }
                            catch (Exception ex)
                            {
                                this.Invoke(new DisplayEstado(Progreso), "ERROR AL USAR LA CLASE DBFtp" + ex.Message);
                            }
                        }
                        catch (Exception ex)
                        {
                             this.Invoke(new DisplayEstado(Progreso), "ERROR AL ACTUALIZAR REGISTROS EN LA BASE DE DATOS " + conf.rutaFinalXML + " " + ex.Message);
                        }
                        counter++;
                    }
                    this.Invoke(new DisplayEstado(Progreso), "SE PROCESARON  " + counter + " DE " + total + " ARCHIVOS EN " + conf.rutaInicialXML);
                }
                else
                {
                    this.Invoke(new DisplayEstado(Progreso), "DIRECTORIO " + conf.rutaInicialXML + " SIN ARCHIVOS NUEVOS");
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void runTypes(NFSclParamConfigFacturacionDTO conf)
        {
            try
            {              
                XmlDocument xDoc = new XmlDocument();
                XslTransform xsl;

                string archivoActual = "";
                int counter = 0;
                int total = 0;

                ValidateDirectory(conf.rutaInicialXML);
                ValidateDirectory(conf.rutaInicialXML + "\\Respaldo\\");
                ValidateDirectory(conf.rutaFinalXML);
                string[] pdfFiles = GetFileNames(conf.rutaInicialXML, "*.xml");// RESCATA LOS ARCHIVOS XML   

                if (pdfFiles.Length > 0)
                {
                    total = pdfFiles.Length;
                    this.Invoke(new DisplayEstado(Progreso), "SE ENCONTRARON " + total + " ARCHIVOS NUEVOS  EN " + conf.rutaInicialXML);
                    for (int i = 0; i < pdfFiles.Length; i++)
                    {
                        archivoActual = pdfFiles[i];
                        this.Invoke(new DisplayEstado(Progreso), "PROCESANDO " + archivoActual);
                        xsl = new XslTransform();
                        if (!String.IsNullOrEmpty(conf.rutaXslt))
                        {
                            try
                            {
                                xsl.Load(conf.rutaXslt);
                            }
                            catch (Exception ex)
                            {
                                this.Invoke(new DisplayEstado(Progreso), "ERROR RUTA XSLT " + ex.Message);
                            }
                        }

                        String nombreArchivoFinal = DateTime.Now.ToString(conf.nombreArchivoXML) + conf.formatoDoc;
                        String xmlPath = conf.rutaInicialXML + "\\" + archivoActual;//((FileInfo)files[i]).FullName;
                        String[] xmlPathData = xmlPath.Split('_');
                        String folio = String.Empty;
                        if (xmlPathData.Count(s => s.Contains("folio$")) > 0)
                        {
                            folio = xmlPathData.Single(s => s.Contains("folio$"));
                            folio = folio.Replace("folio$", "");
                        }
                        nombreArchivoFinal = nombreArchivoFinal.Replace("$folio", folio);
                        //Si tiene ruta Xslt
                        if (!String.IsNullOrEmpty(conf.rutaXslt) && xmlPath.EndsWith(".xml"))
                        {
                            xsl.Transform(xmlPath, conf.rutaFinalXML + "\\" + nombreArchivoFinal);//fechaActualDia + fechaActualMes + fechaActualAño + "_" + fechaActualHora + fechaActualMinuto + fechaActualSegundo + conf.formato);//+ ((FileInfo)files[i]).Name.Replace(".xml", conf.formato));
                            Thread.Sleep(1000); // descanso entre cada documento generado para que el servido lo procese
                        }
                        else
                        {
                            File.Copy(xmlPath, conf.rutaFinalXML + "\\" + nombreArchivoFinal);
                        }
                        this.Invoke(new DisplayEstado(Progreso), "ARCHIVO GENERADO CORRECTAMENTE");
                        this.Invoke(new DisplayEstado(Progreso), "RUTA DEL ARCHIVO A MOVER " + conf.rutaInicialXML + "\\" + archivoActual);
                        this.Invoke(new DisplayEstado(Progreso), "RUTA DONDE SE MOVERA EL ARCHIVO " + conf.rutaFinalXML + "\\Respaldo\\" + archivoActual);

                        if (File.Exists(conf.rutaInicialXML + "\\Respaldo\\" + archivoActual))
                        {
                            File.Delete(conf.rutaInicialXML + "\\Respaldo\\" + archivoActual);
                        }
                        else
                        {
                            File.Move(conf.rutaInicialXML + "\\" + archivoActual, conf.rutaInicialXML + "\\Respaldo\\" + archivoActual);
                        }
                        try
                        {
                            if (string.IsNullOrEmpty(conf.nomInterfaz))
                                throw new Exception("La interface para el laboratorio: " + conf.idLaboratorio.ToString() + " no contiene un nombre de interfaz definido en la tabla, debe definirse en Administracion de sistema -> Configuracion de interfaces ");

                            try
                            {
                                FtpDTO ftpItem = new FtpDTO(); 
                                FTPDAL ftpDal=new FTPDAL();

                                ftpItem.Ftp = conf.nomInterfaz;
                                ftpItem.Path = conf.rutaFinalXML + @"\";
                                ftpItem.Name = nombreArchivoFinal;
                                ftpItem.Status = "CRE";
                                ftpItem.ProcessName = "Creacion archivos desde servicio NFSInterfaceService";
                                ftpItem.Obs = $"Laboratorio { conf.idLaboratorio.ToString()} --> {archivoActual}";
                               
                                ftpDal.SetFtpRecord(ftpItem);
                            }
                            catch (Exception ex)
                            {
                                this.Invoke(new DisplayEstado(Progreso), "ERROR AL USAR LA CLASE DBFtp" + ex.Message);
                            }
                        }
                        catch (Exception ex)
                        {
                           this.Invoke(new DisplayEstado(Progreso), "ERROR AL ACTUALIZAR REGISTROS EN LA BASE DE DATOS " + conf.rutaFinalXML + " " + ex.Message);
                        }

                        counter++;
                    }
                    this.Invoke(new DisplayEstado(Progreso), "SE PROCESARON  " + counter + " DE " + total + " ARCHIVOS EN " + conf.rutaInicialXML);

                }
                else
                {
                    this.Invoke(new DisplayEstado(Progreso), "DIRECTORIO " + conf.rutaInicialXML + " SIN ARCHIVOS NUEVOS");
                }

            }
            catch (Exception ex)
            {
                this.Invoke(new DisplayEstado(Progreso), "ERROR EN EL METODO recorrerDirectorio_Tipo_1_o_2  " + ex.Message);
            }
        }

     
        public void Progreso(string msg)
        {
            string enter = char.ConvertFromUtf32(13) + char.ConvertFromUtf32(10);
            string ant_Text = txb_msg.Text;
            if (ant_Text.Length > 1024)
            {
                this.Invoke(new CleanDisplay(clearScreen));
            }
            txb_msg.Text = msg + enter + ant_Text;

            lf.WriteTxtLog("LOG_MONITOR_FACTURACION_DBNET.TXT", ant_Text);
        }

        private void limpiaMsg()
        {
            txb_msg.Text = "";
        }
        private static string[] GetFileNames(string carpeta, string extencionDocto)// busca todos los xml de una carpeta
        {
            string[] files = Directory.GetFiles(carpeta, extencionDocto);
            for (int i = 0; i < files.Length; i++)
                files[i] = Path.GetFileName(files[i]);
            return files;
        }           

        private void btnFtp_Click(object sender, EventArgs e)
        {
            hilo3.Interval = 1;
            hilo3.Start();
            hilo3.Elapsed += new ElapsedEventHandler(tercer_proceso);
            hilo3.Enabled = true;
        }

     

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.Invoke(new CleanDisplay(clearScreen));
        }

        /// <summary>
        /// Limpia la pantalla
        /// </summary>
        /// <param name=""></param>
        private void clearScreen()
        {
            txb_msg.Text = "";
        }                    

        private void numUpDwn_ValueChanged(object sender, EventArgs e)
        {
            processMinutes = Convert.ToInt32(this.Invoke(new GetIntervalTime(GetTimeIteration)));
        }

        /// <summary>
        /// Obtiene minutos de intervalo 
        /// </summary>
        /// <param name=""></para
        public int GetTimeIteration()
        {
            int intTime = Convert.ToInt32(numUpDwn.Value);
            return (intTime == 0) ? 1 : intTime;
        }

        private void btn_logs_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", lf.GetPathLogProcessString());
        }
    }
}
