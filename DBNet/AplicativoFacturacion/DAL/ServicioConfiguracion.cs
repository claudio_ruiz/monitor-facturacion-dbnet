﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AplicativoFacturacion.NFSseInterfaceConfiguration;

namespace AplicativoFacturacion.Models
{
    public class ServicioConfiguracion
    {
        /// <summary>
        /// Verificado si el diario es valido
        /// </summary>
        /// <param name="_diario"></param>
        /// <returns></returns>
        public List<NFSclParamConfigFacturacionDTO> GetInterfaceConfiguration(int _idInterface = 1)
        {
            InterfaceConfigurationServiceClient _cliente = new InterfaceConfigurationServiceClient();
            List<NFSclParamConfigFacturacionDTO> _lista =new  List<NFSclParamConfigFacturacionDTO>();
            try
            {
                _lista = _cliente.getInterfaceConfiguration(null,_idInterface).ToList();
                _cliente.Close();
                return _lista;
            }
            catch (Exception ex)
            {
                _cliente.Abort();
                return null;
            }
            return _lista;
        }
    }
}
