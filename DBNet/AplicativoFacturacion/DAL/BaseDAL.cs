﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DataDAL
{
    public abstract class BaseDAL
    {
        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["connString"]].ConnectionString;
        }

        public string GetInterfaceMonitorConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["connStringInterfaceMonitor"]].ConnectionString;
        }
    }
}
