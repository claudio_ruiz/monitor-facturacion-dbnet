﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AplicativoFacturacion.Helpers
{
    public class NpoiHelper
    {
        ISheet sheet;
        IRow row;
        ICell cell;

        public NpoiHelper(ISheet sheet)
        {
            this.sheet = sheet;
        }
        public String GetValue(int row, int cell)
        {
            this.row = sheet.GetRow(row);
            if (this.row == null) return String.Empty;
            this.cell = this.row.GetCell(cell);
            if (this.cell == null) return String.Empty;
            this.cell.SetCellType(CellType.STRING);
            return this.cell.StringCellValue;
        }
        public void SetValue(int row, int cell, string value)
        {
            this.row = sheet.GetRow(row);
            if (this.row == null) this.row = sheet.CreateRow(row);
            this.cell = this.row.GetCell(cell);
            if (this.cell == null) this.cell = this.row.CreateCell(cell);


            int result;
            if (Int32.TryParse(value, out result))
            {
                this.cell.SetCellType(CellType.NUMERIC);
                this.cell.SetCellValue(result);
            }
            else
            {
                this.cell.SetCellType(CellType.STRING);
                this.cell.SetCellValue(value);
            }
        }
   
    }
}
