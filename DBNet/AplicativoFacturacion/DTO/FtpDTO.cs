﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorDTO
{
    public class FtpDTO
    {
        public Int64 Id { get; set; }
        public String Ftp { get; set; }
        public String Path { get; set; }
        public String Name { get; set; }
        public String Status { get; set; }
        public String Obs { get; set; }
        public DateTime CrtdTime { get; set; }
        public DateTime SentDateTime { get; set; }
        public String ProcessName { get; set; }
    }
}
