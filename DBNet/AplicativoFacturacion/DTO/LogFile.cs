﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Text;

namespace MonitorDTO
{
    public class LogFile
    {
        private string root = string.Empty;
        string backupFolder = string.Empty;
        public void WriteTxtLog(
            string fileName,
            string text)
        {
            try
            {
                root = GetPathLogProcessString();
                backupFolder = GetBackupFolderString();

                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                if (!Directory.Exists( backupFolder))
                {
                    Directory.CreateDirectory(backupFolder);
                }

                System.IO.StreamWriter sw = new System.IO.StreamWriter(root + fileName, true);
                sw.WriteLine(text);
                sw.Dispose();
                sw.Close();
            }
            catch (Exception ex) { }
            finally {
                ValidaMaximo( root,fileName,backupFolder);
            }
        }

        public void ValidaMaximo(string root, string archivo, string anteriores)
        {
            long size = 0;
            long sizeCompare = 0;
            string path = root + archivo;
            string fecha = "";
            try
            {
                FileInfo fi1 = new FileInfo(path);
                size = fi1.Length;
                sizeCompare = long.Parse(GetLogSize());
                if (size >= sizeCompare)
                {
                    fecha = System.DateTime.Now.ToString();
                    fecha = fecha.Replace("/", "");
                    fecha = fecha.Replace("-", "");
                    fecha = fecha.Replace(".", "");
                    fecha = fecha.Replace(":", "");
                    fecha = fecha.Replace(" ", "");
                    File.Move(path,  anteriores + fecha.Trim() + "_" + archivo);
                }


            }
            catch (Exception ex)
            {

            }
        }

        public string GetLogSize()
        {
            return ConfigurationManager.AppSettings["LogSize"].ToString();
        }
        public string GetPathLogProcessString()
        {
            return ConfigurationManager.AppSettings["PathLogProcess"].ToString();
        }
        public string GetBackupFolderString()
        {
            return ConfigurationManager.AppSettings["BKPPathProcess"].ToString();
        }
    }
}
