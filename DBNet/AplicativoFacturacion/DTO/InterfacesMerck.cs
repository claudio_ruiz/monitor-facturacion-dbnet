﻿using AplicativoFacturacion.Helpers;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AplicativoFacturacion.Models
{
    public class InterfacesMerck
    {
        public static void CreaExcelStock(String xmlPath, String templateXls, String excelOut)
        {
            //XElement xelement = XElement.Load(@"\\srvaos01\Interfaces\Elanco\Stock\Procesamiento\In\Respaldo\2015 01 30_10 38 22.xml");
            XElement xelement = XElement.Load(xmlPath);
            string sendDate = xelement.Element("SendDate").Value;
            string sendDateFull = xelement.Element("SendDateFull").Value;

            IEnumerable<XElement> stocks = xelement.Elements("Stock");

            byte[] excelTemplate = File.ReadAllBytes(templateXls);

            using (FileStream fs = File.Create(excelOut))
            {
                fs.Write(excelTemplate, 0, excelTemplate.Length);
            }

            HSSFWorkbook hssfworkbook;

            using (FileStream file = new FileStream(templateXls, FileMode.Open, FileAccess.ReadWrite))
            {
                hssfworkbook = new HSSFWorkbook(file);
            }
            ISheet sheet = hssfworkbook.GetSheetAt(0);
            NpoiHelper npoi = new NpoiHelper(sheet);

            npoi.SetValue(0, 0, sendDateFull);
            int rowCount = 1;
            string lastItemIdBatch = String.Empty;

            foreach (var stock in stocks)
            {
                if (lastItemIdBatch != (stock.Element("ItemId").Value + stock.Element("Batch").Value))
                {
                    rowCount++;
                    npoi.SetValue(rowCount, 3, "0");
                    npoi.SetValue(rowCount, 4, "0");
                    npoi.SetValue(rowCount, 5, "0");
                    npoi.SetValue(rowCount, 6, "0");
                    npoi.SetValue(rowCount, 7, "0");
                    npoi.SetValue(rowCount, 8, "0");
                    npoi.SetValue(rowCount, 9, "0");
                    npoi.SetValue(rowCount, 10, "0");
                    npoi.SetValue(rowCount, 11, "0");
                    npoi.SetValue(rowCount, 12, "0");
                }

                switch (stock.Element("Warehouse").Value)
                {
                    case "GB":
                        npoi.SetValue(rowCount, 3, stock.Element("Qty").Value);
                        break;
                    case "BAJA":
                        npoi.SetValue(rowCount, 4, stock.Element("Qty").Value);
                        break;
                    case "CUARENT":
                        npoi.SetValue(rowCount, 5, stock.Element("Qty").Value);
                        break;
                    case "MERMA":
                        npoi.SetValue(rowCount, 6, stock.Element("Qty").Value);
                        break;
                    case "CANJES":
                        npoi.SetValue(rowCount, 7, stock.Element("Qty").Value);
                        break;
                    case "DEVOLUC":
                        npoi.SetValue(rowCount, 8, stock.Element("Qty").Value);
                        break;
                    case "JUMBO":
                        npoi.SetValue(rowCount, 9, stock.Element("Qty").Value);
                        break;
                    case "RECALL":
                        npoi.SetValue(rowCount, 10, stock.Element("Qty").Value);
                        break;
                    case "SECUARENT":
                        npoi.SetValue(rowCount, 11, stock.Element("Qty").Value);
                        break;
                    case "TERCUARENT":
                        npoi.SetValue(rowCount, 12, stock.Element("Qty").Value);
                        break;
                }

                npoi.SetValue(rowCount, 0, "ACTIVO");
                npoi.SetValue(rowCount, 1, stock.Element("ItemId").Value.Substring(1));
                npoi.SetValue(rowCount, 2, stock.Element("Batch").Value);

                npoi.SetValue(rowCount, 13, stock.Element("ItemGroupId").Value);
            }

            using (var fileData = new FileStream(excelOut, FileMode.Create))
            {
                hssfworkbook.Write(fileData);
            }
        }
   
    }
}
